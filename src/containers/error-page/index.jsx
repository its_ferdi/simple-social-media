import React,  { Component } from 'react';

export default class NoMatchPage extends Component {
	render() {
		return(<div><h1>Ooops, sorry page not found</h1></div>)
	}
}