import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';

import Loadable from 'react-loadable';


class LoaderBar extends Component {
	render() {
		return(<div><p>Please wait ...</p></div>)
	}
}

const AsyncHome = Loadable({
	loader 		: () => import('containers/home'),
	loading  	: LoaderBar
})

const AsyncUser = Loadable({
	loader 		: () => import('containers/user'),
	loading  	: LoaderBar
})

const AsyncUserDetail = Loadable({
	loader 		: () => import('containers/user/detail'),
	loading  	: LoaderBar
})

const AsyncPost = Loadable({
	loader 		: () => import('containers/post'),
	loading  	: LoaderBar
})

const AsyncPostDetail = Loadable({
	loader 		: () => import('containers/post/detail'),
	loading  	: LoaderBar
})

const AsyncNotMatch = Loadable({
	loader 		: () => import('containers/error-page'),
	loading  	: LoaderBar
})

export class WebRouter extends Component {

	render() {

		return(
			<Switch>
				{/*default root*/}
				<Route exact path="/" component={AsyncHome} />
				
				{/*User*/}
				<Route exact path="/users" component={AsyncUser} />
				<Route path="/users/:id?" component={AsyncUserDetail} />
				
				{/*Post*/}
				<Route exact path="/posts" component={AsyncPost} />
				<Route path="/:user_id/:slug_url" component={AsyncPostDetail} />
				
				{/*404*/}
				<Route component={AsyncNotMatch} />
			</Switch>
		);
	}
} 

export default withRouter(WebRouter);
